package umn.ac.id.myapplication;

import android.os.AsyncTask;
import android.util.Log;

public class CustomServiceTask extends AsyncTask<Integer, Integer, Integer> {
    @Override
    protected void onProgressUpdate(Integer... integers) {
        Log.i("CUSTOMSERVICE", "onStartCommand: " + integers[0] + " berjalan " +
                integers[1] + " persen");
    }

    @Override
    protected Integer doInBackground(Integer... integers) {
        return null;
    }

    @Override
    protected void onPostExecute(Integer result) {
        Log.i("CUSTOMSERVICE", "onStartCommand: " + result + " Selesai");
    }
}